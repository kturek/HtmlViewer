package com.tmobisoft.htmlviewer

import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.BeforeClass
import org.junit.Rule
import org.mockito.junit.MockitoJUnit
import java.util.concurrent.Executor

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class BaseUnitTest {

    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    companion object {
        @BeforeClass
        @JvmStatic
        fun setUpRxSchedulers() {

            val immediate = object : Scheduler() {
                override fun createWorker() = ExecutorScheduler.ExecutorWorker(Executor { it.run() })
            }

            RxJavaPlugins.setInitIoSchedulerHandler { _ -> immediate }
            RxJavaPlugins.setInitComputationSchedulerHandler { _ -> immediate }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { _ -> immediate }
            RxJavaPlugins.setInitSingleSchedulerHandler { _ -> immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> immediate }
        }
    }
}