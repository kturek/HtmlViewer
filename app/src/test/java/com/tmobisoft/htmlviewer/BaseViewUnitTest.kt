package com.tmobisoft.htmlviewer

import android.content.Intent
import com.tmobisoft.htmlviewer.presentation.TestApp
import com.tmobisoft.htmlviewer.presentation.mvp.MvpActivity
import com.tmobisoft.htmlviewer.presentation.mvp.MvpPresenter
import com.tmobisoft.htmlviewer.presentation.mvp.MvpView
import org.junit.Before
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, application = TestApp::class)
abstract class BaseViewUnitTest<
        out TestedView : MvpView,
        out Presenter : MvpPresenter<TestedView>,
        Activity : MvpActivity<Presenter, TestedView>> {

    abstract val testActivityClass: Class<Activity>
    private lateinit var testedActivity: Activity

    val presenter: Presenter
        get() = testedActivity.presenter
    val testedView: TestedView
        get() = testedActivity.presentedView

    @Before
    fun setUp() {
        testedActivity = Robolectric.buildActivity(testActivityClass, setupIntent(Intent())).create().get()
        prepareForTest()
    }

    open fun prepareForTest() {}

    open fun setupIntent(intent: Intent) = intent
}