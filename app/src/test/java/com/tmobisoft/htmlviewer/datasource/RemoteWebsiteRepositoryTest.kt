package com.tmobisoft.htmlviewer.datasource

import com.nhaarman.mockito_kotlin.verify
import com.tmobisoft.htmlviewer.BaseUnitTest
import com.tmobisoft.htmlviewer.domain.htmlviewer.FileHandler
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import retrofit2.Response
import java.io.File
import org.mockito.Mockito.`when` as whenDo

/**
 * Created by Krzysztof Turek on 01.10.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
class RemoteWebsiteRepositoryTest : BaseUnitTest() {

    @Mock lateinit var service: WebsiteService
    @Mock lateinit var fileHandler: FileHandler
    @InjectMocks lateinit var testedRepository: RemoteWebsiteRepository

    @Test
    fun shouldReceiveWebsiteFromServiceAndSaveIt() {
        // given
        val testUrl = "http://t-mobisoft.com"
        val response = Response.success(ResponseBody.create(MediaType.parse("MediaType"),"content"))
        val file = File("Path")
        val bodyParts = listOf("A", "B", "C")
        whenDo(service.getWebsiteFor(testUrl)).thenReturn(Observable.just(response))
        whenDo(fileHandler.websiteUrl(testUrl)).thenReturn(fileHandler)
        whenDo(fileHandler.save(response.body())).thenReturn(file)
        whenDo(fileHandler.load(file)).thenReturn(bodyParts)

        // when
        val testRepositoryObserver = testedRepository.getWebsite(testUrl).test()
        testRepositoryObserver.awaitTerminalEvent()

        // given
        testRepositoryObserver
                .assertComplete()
                .assertNoErrors()
                .assertValue { it.bodyParts == bodyParts }
        verify(fileHandler).save(response.body())
    }
}