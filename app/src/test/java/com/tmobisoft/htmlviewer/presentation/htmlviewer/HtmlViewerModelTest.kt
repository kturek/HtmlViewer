package com.tmobisoft.htmlviewer.presentation.htmlviewer

import com.tmobisoft.htmlviewer.BaseUnitTest
import com.tmobisoft.htmlviewer.domain.htmlviewer.model.Website
import com.tmobisoft.htmlviewer.domain.htmlviewer.usecase.GetWebsiteUseCase
import io.reactivex.Observable
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when` as whenDo

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
class HtmlViewerModelTest : BaseUnitTest() {

    @Mock lateinit var getWebsiteFromNetworkUseCase: GetWebsiteUseCase
    @Mock lateinit var getWebsiteFromLocalStorageUseCase: GetWebsiteUseCase
    @InjectMocks lateinit var testedModel: HtmlViewerModel

    @Test
    fun shouldLoadWebsiteFromCacheIfItIsAvailable() {
        // given
        val testUrl = "Test url"
        val cachedWebsite = Website(bodyParts = listOf("A", "B", "C"))
        whenDo(getWebsiteFromLocalStorageUseCase.url(testUrl)).thenReturn(getWebsiteFromLocalStorageUseCase)
        whenDo(getWebsiteFromLocalStorageUseCase.execute()).thenReturn(Observable.just(cachedWebsite))
        val website = Website(bodyParts = listOf("D", "E"))
        whenDo(getWebsiteFromNetworkUseCase.url(testUrl)).thenReturn(getWebsiteFromNetworkUseCase)
        whenDo(getWebsiteFromNetworkUseCase.execute()).thenReturn(Observable.just(website))

        // when
        val testModelObserver = testedModel.getWebsite(testUrl).test()
        testModelObserver.awaitTerminalEvent()

        // then
        testModelObserver
                .assertNoErrors()
                .assertComplete()
                .assertValue { it == cachedWebsite }
                .assertValue { it != website }
    }
}