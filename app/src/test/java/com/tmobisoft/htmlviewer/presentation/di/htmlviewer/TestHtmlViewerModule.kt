package com.tmobisoft.htmlviewer.presentation.di.htmlviewer

import com.tmobisoft.htmlviewer.presentation.htmlviewer.*
import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
@Module
class TestHtmlViewerModule {

    @Provides
    fun providesBodyPartsAdapter() = BodyPartsAdapter()

    @Provides
    fun providesHtmlViewerView(adapter: BodyPartsAdapter, activity: HtmlViewerActivity) = HtmlViewerView(adapter, activity)

    @Provides
    fun providesHtmlViewerModel() = mock(HtmlViewerModel::class.java)

    @Provides
    fun providesHtmlViewerPresenter() = mock(HtmlViewerPresenter::class.java)
}