package com.tmobisoft.htmlviewer.presentation.di

import com.tmobisoft.htmlviewer.presentation.TestApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
@Singleton
@Component(modules = arrayOf(
        AndroidSupportInjectionModule::class,
        TestAppModule::class,
        TestBindingModule::class
))
interface TestAppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: TestApp): Builder

        fun build(): TestAppComponent
    }

    fun inject(app: TestApp)
}