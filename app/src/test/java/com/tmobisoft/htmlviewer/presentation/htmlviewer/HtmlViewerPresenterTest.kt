package com.tmobisoft.htmlviewer.presentation.htmlviewer

import com.nhaarman.mockito_kotlin.verify
import com.tmobisoft.htmlviewer.BaseUnitTest
import com.tmobisoft.htmlviewer.domain.htmlviewer.model.Website
import com.tmobisoft.htmlviewer.presentation.common.UrlValidator
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.anyString
import org.mockito.Mockito.`when` as whenDo

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
class HtmlViewerPresenterTest : BaseUnitTest() {

    @Mock lateinit var model: HtmlViewerModel
    @Mock lateinit var urlValidator: UrlValidator
    @Mock lateinit var view: HtmlViewerView
    private val incorrectUrlMessage = "Test incorrect url message"

    private lateinit var testedPresenter: HtmlViewerPresenter

    @Before
    fun setUp() {
        testedPresenter = HtmlViewerPresenter(model, urlValidator, incorrectUrlMessage, view)

        whenDo(urlValidator.isValid(anyString())).thenReturn(true)
    }

    @Test
    fun shouldDisplayIncorrectUrlMessageWhenUrlIsNotValid() {
        // given
        val incorrectUrl = "Test"
        whenDo(urlValidator.isValid(anyString())).thenReturn(false)

        // when
        testedPresenter.getWebsite(incorrectUrl)

        // then
        verify(view).displayError(incorrectUrlMessage)
    }

    @Test
    fun shouldDisplayProgressAndDisableInputViewWhenStartLoading() {
        // given
        val testUrl = "Test"
        whenDo(model.getWebsite(testUrl)).thenReturn(Single.just(Website(bodyParts = emptyList())))

        // when
        testedPresenter.getWebsite(testUrl)

        // then
        verify(view).displayProgress()
        verify(view).setInputState(enable = false)
    }

    @Test
    fun shouldDisplayContentAndEnableInputViewWhenFinishLoading() {
        // given
        val testUrl = "Test"
        whenDo(model.getWebsite(testUrl)).thenReturn(Single.just(Website(bodyParts = emptyList())))

        // when
        testedPresenter.getWebsite(testUrl)

        // then
        verify(view).displayContent()
        verify(view).setInputState(enable = true)
    }

    @Test
    fun shouldPassWebsiteBodyToViewAndClearInputViewWhenReceiveResponse() {
        // given
        val testUrl = "Test"
        val bodyParts = listOf("Part A", "Part B", "Part C")
        whenDo(model.getWebsite(testUrl)).thenReturn(Single.just(Website(bodyParts = bodyParts)))

        // when
        testedPresenter.getWebsite(testUrl)

        // then
        verify(view).clearInput()
        verify(view).setWebsiteBody(bodyParts)
    }

    @Test
    fun shouldDisplayErrorOnView() {
        // given
        val testUrl = "Test"
        val errorMessage = "Test error"
        val exception = Throwable(errorMessage)
        whenDo(model.getWebsite(testUrl)).thenReturn(Single.error(exception))

        // when
        testedPresenter.getWebsite(testUrl)

        // then
        verify(view).displayError(errorMessage)
    }
}