package com.tmobisoft.htmlviewer.presentation.htmlviewer

import com.nhaarman.mockito_kotlin.verify
import com.tmobisoft.htmlviewer.BaseViewUnitTest
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mockito.anyString

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
class HtmlViewerViewTest : BaseViewUnitTest<HtmlViewerView, HtmlViewerPresenter, HtmlViewerActivity>() {

    override val testActivityClass = HtmlViewerActivity::class.java

    @Test
    fun shouldHasAdapterSetOnStart() {
        assertTrue(testedView.websiteRecyclerBodyView.adapter is BodyPartsAdapter)
    }

    @Test
    fun shouldHasEmptyAdapterOnStart() {
        assertTrue(testedView.websiteRecyclerBodyView.adapter.itemCount == 0)
    }

    @Test
    fun shouldSetBodyParts() {
        // given
        val bodyParts = listOf("A", "B", "C")

        // expected
        assertEquals(testedView.websiteRecyclerBodyView.adapter.itemCount, 0)

        //when
        testedView.setWebsiteBody(bodyParts)

        // then
        assertEquals(testedView.websiteRecyclerBodyView.adapter.itemCount, bodyParts.size)
    }

    @Test
    fun shouldDisplayContent() {
        // when
        testedView.displayContent()

        // then
        assertEquals(testedView.contentViewAnimator.displayedChild, HtmlViewerView.CHILD_INDEX_CONTENT)
    }

    @Test
    fun shouldDisplayProgress() {
        // when
        testedView.displayProgress()

        // then
        assertEquals(testedView.contentViewAnimator.displayedChild, HtmlViewerView.CHILD_INDEX_PROGRESS)
    }

    @Test
    fun shouldClearInput() {
        // given
        val input = "Test input"
        testedView.inputView.textField.setText(input)

        // expected
        assertFalse(testedView.inputView.textField.text.isEmpty())

        // when
        testedView.clearInput()

        // then
        assertTrue(testedView.inputView.textField.text.isEmpty())
    }

    @Test
    fun shouldChangeInputState() {
        // when
        testedView.setInputState(enable = false)

        // then
        assertFalse(testedView.inputView.isEnabled)

        // when
        testedView.setInputState(enable = true)

        // then
        assertTrue(testedView.inputView.isEnabled)
    }

    @Test
    fun shouldDelegateSendButtonClickToPresenter() {
        // when
        testedView.inputView.sendButton.performClick()

        // then
        verify(presenter).getWebsite(anyString())
    }
}