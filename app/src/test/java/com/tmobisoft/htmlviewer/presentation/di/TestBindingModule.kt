package com.tmobisoft.htmlviewer.presentation.di

import android.app.Activity
import com.tmobisoft.htmlviewer.presentation.di.htmlviewer.TestHtmlViewerSubComponent
import com.tmobisoft.htmlviewer.presentation.htmlviewer.HtmlViewerActivity
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
@Module
abstract class TestBindingModule {

    @Binds
    @IntoMap
    @ActivityKey(HtmlViewerActivity::class)
    abstract fun bindPinsActivityInjectorFactory(builder: TestHtmlViewerSubComponent.Builder): AndroidInjector.Factory<out Activity>
}