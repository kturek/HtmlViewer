package com.tmobisoft.htmlviewer.presentation.di

import com.tmobisoft.htmlviewer.presentation.di.htmlviewer.TestHtmlViewerSubComponent
import dagger.Module

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
@Module(subcomponents = arrayOf(
        TestHtmlViewerSubComponent::class
))
open class TestAppModule