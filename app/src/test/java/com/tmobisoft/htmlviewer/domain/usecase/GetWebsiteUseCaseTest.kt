package com.tmobisoft.htmlviewer.domain.usecase

import com.tmobisoft.htmlviewer.BaseUnitTest
import com.tmobisoft.htmlviewer.datasource.WebsiteRepository
import com.tmobisoft.htmlviewer.datasource.model.WebsiteEntity
import com.tmobisoft.htmlviewer.domain.htmlviewer.WebsiteMapper
import com.tmobisoft.htmlviewer.domain.htmlviewer.model.Website
import com.tmobisoft.htmlviewer.domain.htmlviewer.usecase.GetWebsiteUseCase
import io.reactivex.Observable
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.Mockito.`when` as whenDo

/**
 * Created by Krzysztof Turek on 01.10.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
class GetWebsiteUseCaseTest : BaseUnitTest() {

    @Mock lateinit var repository: WebsiteRepository
    @Mock lateinit var mapper: WebsiteMapper
    @InjectMocks lateinit var testedUseCase: GetWebsiteUseCase

    @Test(expected = IllegalStateException::class)
    fun shouldThrowExceptionWhenUrlIsNotProvided() {
        // then
        testedUseCase.execute()
    }

    @Test
    fun shouldGetWebsiteFromRepository() {
        // given
        val testUrl = "http://t-mobisoft.com"
        val websiteEntity = WebsiteEntity(bodyParts = listOf("A", "B", "C"))
        whenDo(repository.getWebsite(testUrl)).thenReturn(Observable.just(websiteEntity))
        val website = Website(bodyParts = listOf("D"))
        whenDo(mapper.map(websiteEntity)).thenReturn(website)

        // when
        val testUseCaseObserver = testedUseCase.url(testUrl).execute().test()
        testUseCaseObserver.awaitTerminalEvent()

        // then
        verify(repository).getWebsite(testUrl)
        verify(mapper).map(websiteEntity)
        testUseCaseObserver
                .assertNoErrors()
                .assertComplete()
                .assertValue { it == website }
    }
}