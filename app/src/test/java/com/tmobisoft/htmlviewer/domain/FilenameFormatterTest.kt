package com.tmobisoft.htmlviewer.domain

import com.tmobisoft.htmlviewer.BaseUnitTest
import com.tmobisoft.htmlviewer.domain.htmlviewer.FilenameFormatter
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Krzysztof Turek on 01.10.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
class FilenameFormatterTest: BaseUnitTest() {

    private val testedFormatter = FilenameFormatter()
    private val expectedValue = "t-mobisoft.com.txt"

    @Test
    fun shouldRemoveSchemeFromFilename() {
        // given
        val valueWePass1 = "http://t-mobisoft.com"
        val valueWePass2 = "https://t-mobisoft.com"

        // when
        val result1 = testedFormatter.format(valueWePass1)
        val result2 = testedFormatter.format(valueWePass2)

        // then
        assertEquals(result1, expectedValue)
        assertEquals(result2, expectedValue)
    }

    @Test
    fun shouldNotBeCaseSensitive() {
        // given
        val valueWePass = "HTTPS://t-MobiSoft.COM"

        // when
        val result = testedFormatter.format(valueWePass)

        // then
        assertEquals(result, expectedValue)
    }

    @Test
    fun shouldRemoveWWWFromAddress() {
        // given
        val valueWePass = "http://www.t-mobisoft.com"

        // when
        val result = testedFormatter.format(valueWePass)

        // then
        assertEquals(result, expectedValue)
    }
}