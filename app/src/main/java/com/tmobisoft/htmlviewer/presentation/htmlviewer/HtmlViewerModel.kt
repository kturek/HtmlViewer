package com.tmobisoft.htmlviewer.presentation.htmlviewer

import com.tmobisoft.htmlviewer.domain.htmlviewer.model.Website
import com.tmobisoft.htmlviewer.domain.htmlviewer.usecase.GetWebsiteUseCase
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class HtmlViewerModel(private val getWebsiteFromNetworkUseCase: GetWebsiteUseCase,
                           private val getWebsiteFromLocalStorageUseCase: GetWebsiteUseCase) {

    open fun getWebsite(url: String): Single<Website> {
        val local = getWebsiteFromLocalStorageUseCase.url(url).execute()
        val network = getWebsiteFromNetworkUseCase.url(url).execute()
        return Observable.concat(local, network).firstOrError()
    }
}