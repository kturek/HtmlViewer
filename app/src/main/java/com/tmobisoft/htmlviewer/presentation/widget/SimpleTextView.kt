package com.tmobisoft.htmlviewer.presentation.widget

import android.content.Context
import android.graphics.Canvas
import android.text.Layout
import android.text.StaticLayout
import android.util.AttributeSet
import android.view.View


/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class SimpleTextView : View {

    private var layout: Layout? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        isClickable = false
    }
    open fun setTextLayout(textLayout: StaticLayout) {
        layout = textLayout
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val measuredHeight = MeasureSpec.getSize(heightMeasureSpec)
        val measuredWidth = MeasureSpec.getSize(widthMeasureSpec)
        val minHeight = layout?.height ?: 0
        val minWidth = layout?.width ?: 0
        val height = Math.max(minHeight, measuredHeight)
        val width = Math.max(minWidth, measuredWidth)
        setMeasuredDimension(width, height)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        layout ?: return
        canvas.save()
        canvas.translate(paddingLeft.toFloat(), paddingRight.toFloat())
        layout?.draw(canvas)
        canvas.restore()
    }
}