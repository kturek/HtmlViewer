package com.tmobisoft.htmlviewer.presentation.htmlviewer

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.LayoutParams
import android.view.ViewGroup
import com.tmobisoft.htmlviewer.presentation.widget.SimpleTextView

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class BodyPartsAdapter : RecyclerView.Adapter<BodyPartViewHolder>() {

    private var bodyParts = emptyList<String>()

    open fun setData(data: List<String>) {
        bodyParts = data
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: BodyPartViewHolder, position: Int) {
        holder.bind(bodyParts[position], position)
    }

    override fun getItemCount() = bodyParts.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BodyPartViewHolder {
        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        val textView = SimpleTextView(parent.context)
        textView.layoutParams = params
        return BodyPartViewHolder(textView)
    }
}