package com.tmobisoft.htmlviewer.presentation.mvp

import android.os.Bundle
import android.view.View

/**
 * Created by Krzysztof Turek on 27.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
interface MvpView {

    val layoutResId: Int
    val viewFinder: MvpView.(Int) -> View?

    fun created(savedInstanceState: Bundle?) {}

    fun started() {}

    fun resumed() {}

    fun paused() {}

    fun stopped() {}

    fun destroyed() {}

    fun instanceSaved(outState: Bundle?) {}
}