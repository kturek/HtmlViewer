package com.tmobisoft.htmlviewer.presentation.common

import com.tmobisoft.htmlviewer.domain.UriScheme

/**
 * Created by Krzysztof Turek on 29.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
class UrlValidator : Validator<String> {

    override fun isValid(value: String): Boolean {
        val lowerCaseValue = value.toLowerCase()
        return lowerCaseValue.startsWith(UriScheme.http)
                || lowerCaseValue.startsWith(UriScheme.https)
    }
}