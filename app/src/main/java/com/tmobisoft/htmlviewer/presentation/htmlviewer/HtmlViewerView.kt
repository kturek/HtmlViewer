package com.tmobisoft.htmlviewer.presentation.htmlviewer

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.RecyclerView
import android.widget.ViewAnimator
import com.tmobisoft.htmlviewer.R
import com.tmobisoft.htmlviewer.presentation.common.bindView
import com.tmobisoft.htmlviewer.presentation.common.displayChild
import com.tmobisoft.htmlviewer.presentation.mvp.BaseMvpView
import com.tmobisoft.htmlviewer.presentation.widget.InputView

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class HtmlViewerView (private val adapter: BodyPartsAdapter,
                           activity: HtmlViewerActivity)
    : BaseMvpView<HtmlViewerPresenter, HtmlViewerActivity>(activity) {

    override val layoutResId = R.layout.activity_html_viewer

    val contentViewAnimator: ViewAnimator by bindView(R.id.contentViewAnimator)
    val inputView: InputView by bindView(R.id.inputView)
    val websiteRecyclerBodyView: RecyclerView by bindView(R.id.websiteBody)

    override fun created(savedInstanceState: Bundle?) {
        websiteRecyclerBodyView.adapter = adapter
        inputView.onSendClicked = { _, url -> presenter.getWebsite(url) }
    }

    open fun setWebsiteBody(websiteBodyParts: List<String>) {
        adapter.setData(websiteBodyParts)
    }

    open fun setInputState(enable: Boolean) {
        inputView.isEnabled = enable
    }

    open fun displayContent() {
        contentViewAnimator.displayChild(CHILD_INDEX_CONTENT)
    }

    open fun displayProgress() {
        contentViewAnimator.displayChild(CHILD_INDEX_PROGRESS)
    }

    open fun displayError(errorMessage: String) {
        Snackbar.make(contentViewAnimator, errorMessage, Snackbar.LENGTH_SHORT).show()
    }

    open fun clearInput() {
        inputView.clear()
    }

    companion object {
        const val CHILD_INDEX_CONTENT = 0
        const val CHILD_INDEX_PROGRESS = 1
    }
}