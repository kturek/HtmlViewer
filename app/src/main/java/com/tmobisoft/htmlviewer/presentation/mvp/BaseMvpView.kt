package com.tmobisoft.htmlviewer.presentation.mvp

import android.view.View

/**
 * Created by Krzysztof Turek on 27.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
abstract class BaseMvpView<
        out PresenterType : MvpPresenter<MvpView>,
        out ActivityType : MvpActivity<PresenterType, *>>(val boundedActivity: ActivityType) : MvpView {

    val presenter: PresenterType
        get() = boundedActivity.presenter

    override val viewFinder: MvpView.(Int) -> View?
        get() = { boundedActivity.findViewById(it) }
}