package com.tmobisoft.htmlviewer.presentation.widget

import android.content.Context
import android.support.annotation.VisibleForTesting
import android.util.AttributeSet
import android.widget.EditText
import android.widget.ImageButton
import android.widget.RelativeLayout
import com.tmobisoft.htmlviewer.R
import com.tmobisoft.htmlviewer.presentation.common.AfterTextChangedWatcher
import com.tmobisoft.htmlviewer.presentation.common.bindView
import com.tmobisoft.htmlviewer.presentation.common.isNotNullOrBlank
import com.tmobisoft.htmlviewer.presentation.common.stringText

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class InputView : RelativeLayout {

    val textField: EditText by bindView(R.id.textField)
    val sendButton: ImageButton by bindView(R.id.sendButton)

    var onSendClicked: ((InputView, String) -> Unit) = { _, _ -> }

    private val textChangedWatcher = AfterTextChangedWatcher { newText ->
        val active = newText.isNotNullOrBlank()
        sendButton.isEnabled = active
        sendButton.isClickable = active
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context)
    }

    private fun initialize(context: Context) {
        inflate(context, R.layout.custome_view_input_view, this)
        setupSendButton()
        if (isInEditMode) {
            textField.setText(R.string.app_name)
        }
    }

    private fun setupSendButton() {
        sendButton.isEnabled = false
        sendButton.isClickable = false
        sendButton.setOnClickListener {
            onSendClicked(this@InputView, textField.stringText.trim())
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        textField.isEnabled = enabled
        sendButton.isEnabled = enabled
    }

    open fun clear() = textField.text.clear()

    @VisibleForTesting
    public override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        textField.addTextChangedListener(textChangedWatcher)
    }

    @VisibleForTesting
    public override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        textField.removeTextChangedListener(textChangedWatcher)
    }
}