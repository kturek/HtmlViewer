package com.tmobisoft.htmlviewer.presentation.htmlviewer

import com.tmobisoft.htmlviewer.presentation.mvp.MvpActivity
import javax.inject.Inject

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class HtmlViewerActivity: MvpActivity<HtmlViewerPresenter, HtmlViewerView>() {

    @Inject lateinit var htmlViewerPresenter: HtmlViewerPresenter
    @Inject lateinit var htmlViewerView: HtmlViewerView

    override val presenter: HtmlViewerPresenter
        get() = htmlViewerPresenter

    override val presentedView: HtmlViewerView
        get() = htmlViewerView
}