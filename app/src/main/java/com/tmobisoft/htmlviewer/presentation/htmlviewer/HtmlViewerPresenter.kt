package com.tmobisoft.htmlviewer.presentation.htmlviewer

import com.tmobisoft.htmlviewer.domain.htmlviewer.model.Website
import com.tmobisoft.htmlviewer.presentation.common.UrlValidator
import com.tmobisoft.htmlviewer.presentation.common.plusAssign
import com.tmobisoft.htmlviewer.presentation.mvp.MvpPresenter

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class HtmlViewerPresenter(private val model: HtmlViewerModel,
                               private val urlValidator: UrlValidator,
                               private val incorrectUrlMessage: String,
                               view: HtmlViewerView)
    : MvpPresenter<HtmlViewerView>(view) {

    open fun getWebsite(url: String) {
        if (urlValidator.isValid(url)) {
            compositeDisposable += model
                    .getWebsite(url)
                    .doOnSubscribe { onPreLoadingViewSettings() }
                    .doFinally { onPostLoadingViewSettings() }
                    .subscribe(
                            { handleWebsiteResponse(it) },
                            { handleError(it) }
                    )
        } else {
            presentedView.displayError(incorrectUrlMessage)
        }
    }

    private fun onPreLoadingViewSettings() {
        presentedView.setInputState(enable = false)
        presentedView.displayProgress()
    }

    private fun onPostLoadingViewSettings() {
        presentedView.setInputState(enable = true)
        presentedView.displayContent()
    }

    private fun handleWebsiteResponse(website: Website) {
        presentedView.clearInput()
        presentedView.setWebsiteBody(website.bodyParts)
    }

    private fun handleError(error: Throwable) {
        presentedView.setWebsiteBody(emptyList())
        presentedView.displayError(error.localizedMessage)
    }
}