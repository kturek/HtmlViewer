package com.tmobisoft.htmlviewer.presentation.htmlviewer

import android.content.res.Resources
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.view.View
import com.tmobisoft.htmlviewer.presentation.widget.SimpleTextView

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class BodyPartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val textPaint = TextPaint()
    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels

    init {
        textPaint.color = Color.BLACK
        textPaint.isAntiAlias = true
        textPaint.textSize = 23f
    }

    open fun bind(bodyPart: String, position: Int) {
        with(itemView as SimpleTextView) {
            var layout = TextLayoutCache.get(position)
            if (layout == null) {
                layout = StaticLayout(bodyPart, textPaint, screenWidth, Layout.Alignment.ALIGN_NORMAL, 1f, 0.5f, false)
                TextLayoutCache.put(position, layout)
                setTextLayout(layout)
            } else {
                setTextLayout(layout)
            }
        }
    }
}