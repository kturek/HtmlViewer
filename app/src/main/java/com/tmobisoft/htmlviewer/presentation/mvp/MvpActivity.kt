package com.tmobisoft.htmlviewer.presentation.mvp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection

/**
 * Created by Krzysztof Turek on 27.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
abstract class MvpActivity<
        out Presenter : MvpPresenter<MvpView>,
        out PresentedView : MvpView> : AppCompatActivity() {

    abstract val presenter: Presenter
    abstract val presentedView: PresentedView

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(presentedView.layoutResId)
        presentedView.created(savedInstanceState)
        presenter.attached()
    }

    override fun onStart() {
        super.onStart()
        presentedView.started()
    }

    override fun onResume() {
        super.onResume()
        presentedView.resumed()
    }

    override fun onPause() {
        super.onPause()
        presentedView.paused()
    }

    override fun onStop() {
        super.onStop()
        presentedView.stopped()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        presentedView.instanceSaved(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        presentedView.destroyed()
        presenter.detached()
    }
}