package com.tmobisoft.htmlviewer.presentation.common

import android.text.Editable
import android.text.TextWatcher

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

class AfterTextChangedWatcher(private inline val action: (String) -> Unit) : TextWatcher {

    override fun afterTextChanged(editable: Editable?) {
        action(editable?.toString() ?: "")
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        // NO-OP
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        // NO-OP
    }
}