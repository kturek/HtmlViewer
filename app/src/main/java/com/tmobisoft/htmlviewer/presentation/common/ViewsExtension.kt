package com.tmobisoft.htmlviewer.presentation.common

import android.widget.TextView
import android.widget.ViewAnimator

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

val TextView.stringText: String
    get() = text.toString()

fun String.isNotNullOrBlank() = !isNullOrBlank()

fun ViewAnimator.displayChild(childIndex: Int) {
    if (displayedChild != childCount) displayedChild = childIndex
}