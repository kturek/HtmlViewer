package com.tmobisoft.htmlviewer.presentation.common

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

operator fun CompositeDisposable.plusAssign(subscription: Disposable) {
    add(subscription)
}