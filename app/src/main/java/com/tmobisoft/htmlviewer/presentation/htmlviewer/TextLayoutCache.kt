package com.tmobisoft.htmlviewer.presentation.htmlviewer

import android.text.StaticLayout
import android.util.LruCache

/**
 * Created by Krzysztof Turek on 01.10.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

object TextLayoutCache {

    private val bodyParts = LruCache<Int, StaticLayout>(100)

    fun get(position: Int): StaticLayout? = bodyParts.get(position)

    fun put(position: Int, layout: StaticLayout) {
        bodyParts.put(position, layout)
    }
}