package com.tmobisoft.htmlviewer.presentation.mvp

import android.support.annotation.CallSuper
import android.support.annotation.VisibleForTesting
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Krzysztof Turek on 27.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
abstract class MvpPresenter<out V : MvpView>(val presentedView: V) {

    val compositeDisposable = CompositeDisposable()
        @VisibleForTesting get() = field

    @CallSuper
    open fun attached() {}

    @CallSuper
    open fun detached() {
        compositeDisposable.clear()
    }
}