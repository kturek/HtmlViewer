package com.tmobisoft.htmlviewer.domain.htmlviewer.model

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
data class Website(val bodyParts: List<String>)