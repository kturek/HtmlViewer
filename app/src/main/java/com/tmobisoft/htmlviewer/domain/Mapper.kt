package com.tmobisoft.htmlviewer.domain

/**
 * Created by Krzysztof Turek on 27.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
interface Mapper<in From, out To> {

    fun map(from: From): To
}