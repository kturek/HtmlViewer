package com.tmobisoft.htmlviewer.domain.htmlviewer.usecase

import com.tmobisoft.htmlviewer.datasource.WebsiteRepository
import com.tmobisoft.htmlviewer.domain.UseCase
import com.tmobisoft.htmlviewer.domain.htmlviewer.WebsiteMapper
import com.tmobisoft.htmlviewer.domain.htmlviewer.model.Website
import io.reactivex.Observable


/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class GetWebsiteUseCase(private val remoteRepository: WebsiteRepository,
                             private val mapper: WebsiteMapper,
                             private val url: String?)
    : UseCase<Website> {

    constructor(remoteRepository: WebsiteRepository, mapper: WebsiteMapper)
            : this(remoteRepository, mapper, url = null
    )

    open fun url(url: String) = GetWebsiteUseCase(remoteRepository, mapper, url)

    override fun doWork(): Observable<Website> {
        val url = checkNotNull(url) { "Url can not be null" }
        return remoteRepository.getWebsite(url).map { mapper.map(it) }
    }
}