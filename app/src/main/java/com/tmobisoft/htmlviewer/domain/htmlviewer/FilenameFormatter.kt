package com.tmobisoft.htmlviewer.domain.htmlviewer

import com.tmobisoft.htmlviewer.domain.FormatExtension
import com.tmobisoft.htmlviewer.domain.Formatter
import com.tmobisoft.htmlviewer.domain.UriScheme

/**
 * Created by Krzysztof Turek on 29.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

open class FilenameFormatter : Formatter<String> {

    override fun format(value: String) = value
            .toLowerCase()
            .removePrefix(UriScheme.https)
            .removePrefix(UriScheme.http)
            .removePrefix(WWW) + FormatExtension.txt

    companion object {
        private const val WWW = "www."
    }
}