package com.tmobisoft.htmlviewer.domain.htmlviewer

import com.tmobisoft.htmlviewer.datasource.model.WebsiteEntity
import com.tmobisoft.htmlviewer.domain.Mapper
import com.tmobisoft.htmlviewer.domain.htmlviewer.model.Website

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class WebsiteMapper: Mapper<WebsiteEntity, Website> {

    override fun map(from: WebsiteEntity): Website {
        return Website(
                bodyParts = from.bodyParts
        )
    }
}