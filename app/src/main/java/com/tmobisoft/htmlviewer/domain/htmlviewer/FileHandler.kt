package com.tmobisoft.htmlviewer.domain.htmlviewer

import okhttp3.ResponseBody
import okio.Okio
import java.io.File


/**
 * Created by Krzysztof Turek on 29.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

open class FileHandler(private val filesDir: File,
                       private val filenameFormatter: FilenameFormatter,
                       private val websiteUrl: String?) {

    constructor(filesDir: File, filenameFormatter: FilenameFormatter)
            : this(filesDir, filenameFormatter, websiteUrl = null)

    open fun websiteUrl(websiteUrl: String) = FileHandler(filesDir, filenameFormatter, websiteUrl)

    fun save(responseBody: ResponseBody?): File {
        val filename = checkNotNull(websiteUrl) { "Website url can not be null" }
        val formattedFilename = filenameFormatter.format(filename)
        val file = File(filesDir, formattedFilename)
        if (file.exists().not()) {
            val sink = Okio.buffer(Okio.sink(file))
            sink.writeAll(responseBody?.source())
            sink.close()
        }
        return file
    }

    fun load(file: File): List<String> = readBodyPartsFrom(file)

    private fun readBodyPartsFrom(file: File): List<String> {
        val source = Okio.buffer(Okio.source(file))
        val bodyParts = mutableListOf<String>()
        while (true) {
            val line = source.readUtf8Line() ?: break
            bodyParts.addAll(listOf(line))
        }
        return bodyParts
    }

    fun load(websiteUrl: String): List<String> {
        val formattedFilename = filenameFormatter.format(websiteUrl)
        val cachedFile = File(filesDir, formattedFilename)
        return if (cachedFile.exists().not()) {
            emptyList()
        } else {
            readBodyPartsFrom(cachedFile)
        }
    }
}