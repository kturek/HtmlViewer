package com.tmobisoft.htmlviewer.domain

/**
 * Created by Krzysztof Turek on 29.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
interface Formatter<T> {

    fun format(value: T): T
}