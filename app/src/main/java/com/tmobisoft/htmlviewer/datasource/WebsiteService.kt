package com.tmobisoft.htmlviewer.datasource

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by Krzysztof Turek on 29.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
interface WebsiteService {

    @GET
    fun getWebsiteFor(@Url url: String): Observable<Response<ResponseBody>>
}