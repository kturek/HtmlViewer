package com.tmobisoft.htmlviewer.datasource

import com.tmobisoft.htmlviewer.datasource.model.WebsiteEntity
import com.tmobisoft.htmlviewer.domain.htmlviewer.FileHandler
import io.reactivex.Observable

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class LocalWebsiteRepository(private val fileHandler: FileHandler) : WebsiteRepository {

    override fun getWebsite(url: String): Observable<WebsiteEntity> {
        return Observable
                .fromCallable { WebsiteEntity(fileHandler.load(url)) }
                .filter { (bodyLines) -> bodyLines.isEmpty().not() }
    }
}