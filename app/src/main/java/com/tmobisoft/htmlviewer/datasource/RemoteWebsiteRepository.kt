package com.tmobisoft.htmlviewer.datasource

import com.tmobisoft.htmlviewer.datasource.model.WebsiteEntity
import com.tmobisoft.htmlviewer.domain.htmlviewer.FileHandler
import io.reactivex.Observable

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
open class RemoteWebsiteRepository(private val service: WebsiteService,
                                   private val fileHandler: FileHandler)
    : WebsiteRepository {

    override fun getWebsite(url: String): Observable<WebsiteEntity>
            = service.getWebsiteFor(url)
            .map { response -> fileHandler.websiteUrl(url).save(response.body()) }
            .map { cachedFile -> fileHandler.load(cachedFile) }
            .map { bodyLines -> WebsiteEntity(bodyLines) }
}