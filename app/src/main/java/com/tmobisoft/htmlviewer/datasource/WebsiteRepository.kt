package com.tmobisoft.htmlviewer.datasource

import com.tmobisoft.htmlviewer.datasource.model.WebsiteEntity
import io.reactivex.Observable

/**
 * Created by Krzysztof Turek on 28.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
interface WebsiteRepository {

    fun getWebsite(url: String): Observable<WebsiteEntity>
}