package com.tmobisoft.htmlviewer.di;

import android.app.Activity;

import com.tmobisoft.htmlviewer.di.htmlviewer.HtmlViewerSubComponent;
import com.tmobisoft.htmlviewer.presentation.htmlviewer.HtmlViewerActivity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

@Module
public abstract class BindingModule {

    @Binds
    @IntoMap
    @ActivityKey(HtmlViewerActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindPinsActivityInjectorFactory(HtmlViewerSubComponent.Builder builder);
}
