package com.tmobisoft.htmlviewer.di;

import com.tmobisoft.htmlviewer.domain.htmlviewer.WebsiteMapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

@Module
public class MapperModule {

    @Singleton
    @Provides
    WebsiteMapper providesWebsiteMapper() {
        return new WebsiteMapper();
    }
}
