package com.tmobisoft.htmlviewer.di.htmlviewer;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

@Retention(RetentionPolicy.RUNTIME)
@Scope
public @interface HtmlViewerScope {}