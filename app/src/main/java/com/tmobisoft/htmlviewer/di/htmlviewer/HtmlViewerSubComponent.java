package com.tmobisoft.htmlviewer.di.htmlviewer;

import com.tmobisoft.htmlviewer.presentation.htmlviewer.HtmlViewerActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

@HtmlViewerScope
@Subcomponent(modules = {
        HtmlViewerModule.class
})
public interface HtmlViewerSubComponent extends AndroidInjector<HtmlViewerActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<HtmlViewerActivity> {
    }
}
