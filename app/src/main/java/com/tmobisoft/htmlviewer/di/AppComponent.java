package com.tmobisoft.htmlviewer.di;

import com.tmobisoft.htmlviewer.presentation.App;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

@Singleton
@Component(modules = {
                AndroidSupportInjectionModule.class,
                AppModule.class,
                BindingModule.class,
                DataSourceModule.class,
                UseCaseModule.class,
                MapperModule.class
        })
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(App app);

        AppComponent build();
    }

    void inject(App app);
}
