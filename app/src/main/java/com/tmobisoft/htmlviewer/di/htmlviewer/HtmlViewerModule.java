package com.tmobisoft.htmlviewer.di.htmlviewer;

import com.tmobisoft.htmlviewer.R;
import com.tmobisoft.htmlviewer.domain.htmlviewer.usecase.GetWebsiteUseCase;
import com.tmobisoft.htmlviewer.presentation.common.UrlValidator;
import com.tmobisoft.htmlviewer.presentation.htmlviewer.BodyPartsAdapter;
import com.tmobisoft.htmlviewer.presentation.htmlviewer.HtmlViewerActivity;
import com.tmobisoft.htmlviewer.presentation.htmlviewer.HtmlViewerModel;
import com.tmobisoft.htmlviewer.presentation.htmlviewer.HtmlViewerPresenter;
import com.tmobisoft.htmlviewer.presentation.htmlviewer.HtmlViewerView;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
@Module
public class HtmlViewerModule {

    @HtmlViewerScope
    @Provides
    BodyPartsAdapter providesBodyPartsAdapter() {
        return new BodyPartsAdapter();
    }

    @HtmlViewerScope
    @Provides
    HtmlViewerView providesHtmlViewerView(BodyPartsAdapter adapter, HtmlViewerActivity activity) {
        return new HtmlViewerView(adapter, activity);
    }

    @HtmlViewerScope
    @Provides
    HtmlViewerModel providesHtmlViewerModel(
            @Named("from_network") GetWebsiteUseCase getWebsiteFromNetworkUseCase,
            @Named("from_local_storage") GetWebsiteUseCase getWebsiteFromLocalStorageUseCase
    ) {
        return new HtmlViewerModel(getWebsiteFromNetworkUseCase, getWebsiteFromLocalStorageUseCase);
    }

    @HtmlViewerScope
    @Provides
    HtmlViewerPresenter providesHtmlViewerPresenter(
            HtmlViewerModel model,
            UrlValidator urlValidator,
            HtmlViewerActivity activity,
            HtmlViewerView view
    ) {
        return new HtmlViewerPresenter(model, urlValidator, activity.getString(R.string.incorrect_url), view);
    }
}
