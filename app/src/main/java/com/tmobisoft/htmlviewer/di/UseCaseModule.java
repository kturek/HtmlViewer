package com.tmobisoft.htmlviewer.di;

import com.tmobisoft.htmlviewer.datasource.WebsiteRepository;
import com.tmobisoft.htmlviewer.domain.htmlviewer.WebsiteMapper;
import com.tmobisoft.htmlviewer.domain.htmlviewer.usecase.GetWebsiteUseCase;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

@Module
public class UseCaseModule {

    @Singleton
    @Provides
    @Named("from_network")
    public GetWebsiteUseCase providesGetWebsiteFromNetworkUseCase(
            @Named("remote_repository") WebsiteRepository remoteRepository,
            WebsiteMapper websiteMapper
    ) {
        return new GetWebsiteUseCase(remoteRepository, websiteMapper);
    }

    @Singleton
    @Provides
    @Named("from_local_storage")
    public GetWebsiteUseCase providesGetWebsiteFromLocalStorageUseCase(
            @Named("local_repository") WebsiteRepository remoteRepository,
            WebsiteMapper websiteMapper
    ) {
        return new GetWebsiteUseCase(remoteRepository, websiteMapper);
    }
}
