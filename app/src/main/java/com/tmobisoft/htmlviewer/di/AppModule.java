package com.tmobisoft.htmlviewer.di;

import android.content.Context;

import com.tmobisoft.htmlviewer.di.htmlviewer.HtmlViewerSubComponent;
import com.tmobisoft.htmlviewer.domain.htmlviewer.FileHandler;
import com.tmobisoft.htmlviewer.domain.htmlviewer.FilenameFormatter;
import com.tmobisoft.htmlviewer.presentation.App;
import com.tmobisoft.htmlviewer.presentation.common.UrlValidator;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */

@Module(subcomponents = HtmlViewerSubComponent.class)
public class AppModule {

    @Singleton
    @Provides
    @Named("application_context")
    public Context providesApplicationContext(App app) {
        return app.getApplicationContext();
    }

    @Singleton
    @Provides
    public FilenameFormatter providesFilenameFormatter() {
        return new FilenameFormatter();
    }

    @Singleton
    @Provides
    public UrlValidator providesUrlValidator() {
        return new UrlValidator();
    }

    @Singleton
    @Provides
    public FileHandler providesFileHandler(@Named("application_context") Context context,
                                           FilenameFormatter formatter) {
        return new FileHandler(context.getFilesDir(), formatter);
    }
}
