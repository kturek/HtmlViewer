package com.tmobisoft.htmlviewer.di;

import com.tmobisoft.htmlviewer.BuildConfig;
import com.tmobisoft.htmlviewer.datasource.LocalWebsiteRepository;
import com.tmobisoft.htmlviewer.datasource.RemoteWebsiteRepository;
import com.tmobisoft.htmlviewer.datasource.WebsiteRepository;
import com.tmobisoft.htmlviewer.datasource.WebsiteService;
import com.tmobisoft.htmlviewer.domain.htmlviewer.FileHandler;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

/**
 * Created by Krzysztof Turek on 30.09.2017.
 * Copyright (c) 2017 T-Mobisoft. All rights reserved.
 * Contact: krzysztof.turek@t-mobisoft.com
 */
@Module
public class DataSourceModule {

    @Singleton
    @Provides
    WebsiteService providesWebsiteService() {
        final Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl("http://www.t-mobisoft.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        if (BuildConfig.DEBUG) {
            final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            final OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();
            retrofitBuilder.client(client);
        }
        return retrofitBuilder.build().create(WebsiteService.class);
    }

    @Singleton
    @Provides
    @Named("remote_repository")
    public WebsiteRepository providesRemoteWebsiteRepository(WebsiteService service,
                                                             FileHandler fileHandler
    ) {
        return new RemoteWebsiteRepository(service, fileHandler);
    }

    @Singleton
    @Provides
    @Named("local_repository")
    public WebsiteRepository providesLocalWebsiteRepository(FileHandler fileHandler
    ) {
        return new LocalWebsiteRepository(fileHandler);
    }
}
